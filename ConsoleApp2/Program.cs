﻿/*
 * sendg.cs
 * Send gcode to Ultimaker via serial port.
 * (c) 2011 Peter Brier. pbrier.nl - gmail.com
 *
 * Requires Mono or MS C# to compile (mono-gmcs is only required to compile). For example:
 * # sudo apt-get install mono-gmcs mono-gac mono-utils
 * # gmcs sendg.cs
 * # mono sendg.exe
 *
 * USE: sendg -d -l -c[buffercount] -p[portname] -b[baudrate] [filename]
 * -d             Enable debugging
 * -l             Enable logging
 * -e             Enable build time estimation
 * -c[n]          Set delayed ack count (1 is no delayed ack)
 * -p[name]       Specify portname (COMx on windows, /dev/ttyAMx on linux)
 * -b[baudrate]   Set baudrate (default is 115200)
 * [filename]     The GCODE file to send
 * 
 * Best results are obtained when c>3, however, your firmware serial buffer should be large 
 * enough to hold all these lines
 *
 * Two threads are used (a reader and main thread).
 * Note: we do *NOT* use the async events of the SerialPort object.
 * these have 'issues' on mono (one of the 'issues' is that they don't work).
 *
 * Upto "bufcount" lines are sent, before they are ACK-ed
 * A semaphore is used to sync the threads.
 * We assume all these lines fit in the 128 byte serial buffer of
 * the arduino. An optimized version could cound the actual nr of
 * bytes sent and ACKed to sync the reader/writer.
 * No CRC, line-numbers or resents are implemented.
 * Tested with standard ultimaker and sprinter firmware, 
 * using bufcount=2
 *
 * sendg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * sendg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sendg.  If not, see <http://www.gnu.org/licenses/>.
 */
#define WINDOWS

using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace sendg
{
    class Program
    {
        static Thread r; // reader thread
        static Semaphore sem;
        static SerialPort port;
        static bool debug = false, log = false, progress = false, quit = false;
        static long t0 = 0;
        static TextWriter tw = null;
        private static Stopwatch sw = Stopwatch.StartNew();
        private static long msec() { return sw.ElapsedMilliseconds; }


        static void Main(string[] args)
        {
            string portname = "COM31";
            string filename = null;
            int baudrate = 115200;
            int bufcount = 4; // nr of lines to buffer
            int timeout = 6000;
            long t1 = 0;
            bool realtime = false;


            Console.WriteLine("Port: " + portname + " " + baudrate.ToString() + "bps");
            Console.WriteLine("File: " + filename + " buffer:" + bufcount.ToString());
            Console.WriteLine("Realtime priority: " + (realtime ? "ENABLED" : "DISABLED"));


            if (realtime)
                using (Process p = Process.GetCurrentProcess())
                    p.PriorityClass = ProcessPriorityClass.RealTime;

            try
            {
                // open port and wait for Arduino to boot
                port = new SerialPort(portname, baudrate);
                port.Open();
                port.NewLine = "\n";
                port.DtrEnable = true;
                port.RtsEnable = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("SENDG: Cannot open serial port (Portname=" + portname + ")");
                if (debug)
                    Console.WriteLine(ex.ToString());
                return;
            }
            Thread.Sleep(3000);
            // {
            int xMin = 110 ;
            int xMax = 170;
            int gridX = 4;

            int yMin = 110;
            int yMax = 150;            
            int gridY = 3;

            port.Write("G28 X".Trim() + "\n");
            port.Write("G0 Z0 X100".Trim() + "\n");
            Thread.Sleep(5000);
            port.Write("G28 Y".Trim() + "\n");
            port.Write("G0 Z0 Y100".Trim() + "\n");
            
            
            Thread.Sleep(25000);
            double stepSizeX = (xMax-xMin) / gridX;
            double stepSizeY = (yMax-yMin) / gridY;
            int aaaa = 0;
            for (double x = xMin; x <= xMax; x += stepSizeX)
            {
                if (aaaa % 2 == 0)
                {
                    for (double y = yMin; y <= yMax; y += stepSizeY)
                    {
                        string cmd = "G0 X" + x.ToString("#.##") + " Y" + y.ToString("#.##") + " F4000".Trim() + "\n";
                        port.Write(cmd);

                        Console.WriteLine(cmd);
                        Thread.Sleep(4000);
                        File.WriteAllText("0.txt", "1");
                        Thread.Sleep(2000);
                       // while (File.ReadAllText("Read.txt") == "1")
                        //    continue;
                    }
                }
                else
                {
                    for (double y = yMax; y >= yMin; y -= stepSizeY)
                    {
                        string cmd = "G0 X" + x.ToString("#.##") + " Y" + y.ToString("#.##") + " F4000".Trim() + "\n";
                        port.Write(cmd);

                        Console.WriteLine(cmd);
                        Thread.Sleep(4000);
                        File.WriteAllText("0.txt", "1");
                        Thread.Sleep(2000);
                        //while (File.ReadAllText("Read.txt") == "1")
                        //    continue;
                    }
                }
                aaaa++;
            }

            port.Close();

        }

    }
}