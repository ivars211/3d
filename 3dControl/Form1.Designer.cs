﻿namespace _3dControl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YplusButton = new System.Windows.Forms.Button();
            this.XplusButton = new System.Windows.Forms.Button();
            this.YminusButton = new System.Windows.Forms.Button();
            this.XminusButton = new System.Windows.Forms.Button();
            this.ZplusButton = new System.Windows.Forms.Button();
            this.ZplusplusButton = new System.Windows.Forms.Button();
            this.ZminusButton = new System.Windows.Forms.Button();
            this.ZminusminusButton = new System.Windows.Forms.Button();
            this.HomeButton = new System.Windows.Forms.Button();
            this.xStartField = new System.Windows.Forms.TextBox();
            this.xEndField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.xStepField = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.YstepField = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.YendField = new System.Windows.Forms.TextBox();
            this.yStartField = new System.Windows.Forms.TextBox();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.StartButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.camerIDtextField = new System.Windows.Forms.TextBox();
            this.comPortTextField = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // YplusButton
            // 
            this.YplusButton.Location = new System.Drawing.Point(537, 52);
            this.YplusButton.Name = "YplusButton";
            this.YplusButton.Size = new System.Drawing.Size(75, 23);
            this.YplusButton.TabIndex = 0;
            this.YplusButton.Text = "Y+";
            this.YplusButton.UseVisualStyleBackColor = true;
            this.YplusButton.Click += new System.EventHandler(this.YplusButton_Click);
            // 
            // XplusButton
            // 
            this.XplusButton.Location = new System.Drawing.Point(619, 88);
            this.XplusButton.Name = "XplusButton";
            this.XplusButton.Size = new System.Drawing.Size(75, 23);
            this.XplusButton.TabIndex = 1;
            this.XplusButton.Text = "X+";
            this.XplusButton.UseVisualStyleBackColor = true;
            this.XplusButton.Click += new System.EventHandler(this.XplusButton_Click);
            // 
            // YminusButton
            // 
            this.YminusButton.Location = new System.Drawing.Point(537, 128);
            this.YminusButton.Name = "YminusButton";
            this.YminusButton.Size = new System.Drawing.Size(75, 23);
            this.YminusButton.TabIndex = 2;
            this.YminusButton.Text = "Y-";
            this.YminusButton.UseVisualStyleBackColor = true;
            this.YminusButton.Click += new System.EventHandler(this.YminusButton_Click);
            // 
            // XminusButton
            // 
            this.XminusButton.Location = new System.Drawing.Point(446, 88);
            this.XminusButton.Name = "XminusButton";
            this.XminusButton.Size = new System.Drawing.Size(75, 23);
            this.XminusButton.TabIndex = 3;
            this.XminusButton.Text = "X-";
            this.XminusButton.UseVisualStyleBackColor = true;
            this.XminusButton.Click += new System.EventHandler(this.XminusButton_Click);
            // 
            // ZplusButton
            // 
            this.ZplusButton.Location = new System.Drawing.Point(702, 52);
            this.ZplusButton.Name = "ZplusButton";
            this.ZplusButton.Size = new System.Drawing.Size(75, 23);
            this.ZplusButton.TabIndex = 4;
            this.ZplusButton.Text = "Z+";
            this.ZplusButton.UseVisualStyleBackColor = true;
            this.ZplusButton.Click += new System.EventHandler(this.ZplusButton_Click);
            // 
            // ZplusplusButton
            // 
            this.ZplusplusButton.Location = new System.Drawing.Point(702, 23);
            this.ZplusplusButton.Name = "ZplusplusButton";
            this.ZplusplusButton.Size = new System.Drawing.Size(75, 23);
            this.ZplusplusButton.TabIndex = 5;
            this.ZplusplusButton.Text = "Z ++";
            this.ZplusplusButton.UseVisualStyleBackColor = true;
            this.ZplusplusButton.Click += new System.EventHandler(this.ZplusplusButton_Click);
            // 
            // ZminusButton
            // 
            this.ZminusButton.Location = new System.Drawing.Point(702, 128);
            this.ZminusButton.Name = "ZminusButton";
            this.ZminusButton.Size = new System.Drawing.Size(75, 23);
            this.ZminusButton.TabIndex = 6;
            this.ZminusButton.Text = "Z-";
            this.ZminusButton.UseVisualStyleBackColor = true;
            this.ZminusButton.Click += new System.EventHandler(this.ZminusButton_Click);
            // 
            // ZminusminusButton
            // 
            this.ZminusminusButton.Location = new System.Drawing.Point(702, 157);
            this.ZminusminusButton.Name = "ZminusminusButton";
            this.ZminusminusButton.Size = new System.Drawing.Size(75, 23);
            this.ZminusminusButton.TabIndex = 7;
            this.ZminusminusButton.Text = "Z --";
            this.ZminusminusButton.UseVisualStyleBackColor = true;
            this.ZminusminusButton.Click += new System.EventHandler(this.ZminusminusButton_Click);
            // 
            // HomeButton
            // 
            this.HomeButton.Location = new System.Drawing.Point(384, 12);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(75, 23);
            this.HomeButton.TabIndex = 8;
            this.HomeButton.Text = "Home XY";
            this.HomeButton.UseVisualStyleBackColor = true;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // xStartField
            // 
            this.xStartField.Location = new System.Drawing.Point(12, 26);
            this.xStartField.Name = "xStartField";
            this.xStartField.Size = new System.Drawing.Size(77, 20);
            this.xStartField.TabIndex = 9;
            // 
            // xEndField
            // 
            this.xEndField.Location = new System.Drawing.Point(113, 26);
            this.xEndField.Name = "xEndField";
            this.xEndField.Size = new System.Drawing.Size(83, 20);
            this.xEndField.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "X start";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(110, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "X end";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(212, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "X step";
            // 
            // xStepField
            // 
            this.xStepField.Location = new System.Drawing.Point(215, 26);
            this.xStepField.Name = "xStepField";
            this.xStepField.Size = new System.Drawing.Size(83, 20);
            this.xStepField.TabIndex = 13;
            this.xStepField.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(212, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Y step";
            // 
            // YstepField
            // 
            this.YstepField.Location = new System.Drawing.Point(215, 79);
            this.YstepField.Name = "YstepField";
            this.YstepField.Size = new System.Drawing.Size(83, 20);
            this.YstepField.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(110, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Y end";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Y start";
            // 
            // YendField
            // 
            this.YendField.Location = new System.Drawing.Point(113, 79);
            this.YendField.Name = "YendField";
            this.YendField.Size = new System.Drawing.Size(83, 20);
            this.YendField.TabIndex = 16;
            // 
            // yStartField
            // 
            this.yStartField.Location = new System.Drawing.Point(12, 79);
            this.yStartField.Name = "yStartField";
            this.yStartField.Size = new System.Drawing.Size(77, 20);
            this.yStartField.TabIndex = 15;
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(14, 140);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(75, 23);
            this.ConnectButton.TabIndex = 21;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(113, 140);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 22;
            this.StartButton.Text = "Start scan";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Start 3D scan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // camerIDtextField
            // 
            this.camerIDtextField.Location = new System.Drawing.Point(113, 169);
            this.camerIDtextField.Name = "camerIDtextField";
            this.camerIDtextField.Size = new System.Drawing.Size(62, 20);
            this.camerIDtextField.TabIndex = 24;
            // 
            // comPortTextField
            // 
            this.comPortTextField.Location = new System.Drawing.Point(12, 169);
            this.comPortTextField.Name = "comPortTextField";
            this.comPortTextField.Size = new System.Drawing.Size(64, 20);
            this.comPortTextField.TabIndex = 25;
            this.comPortTextField.Text = "COM23";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 193);
            this.Controls.Add(this.comPortTextField);
            this.Controls.Add(this.camerIDtextField);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.YstepField);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.YendField);
            this.Controls.Add(this.yStartField);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.xStepField);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.xEndField);
            this.Controls.Add(this.xStartField);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.ZminusminusButton);
            this.Controls.Add(this.ZminusButton);
            this.Controls.Add(this.ZplusplusButton);
            this.Controls.Add(this.ZplusButton);
            this.Controls.Add(this.XminusButton);
            this.Controls.Add(this.YminusButton);
            this.Controls.Add(this.XplusButton);
            this.Controls.Add(this.YplusButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YplusButton;
        private System.Windows.Forms.Button XplusButton;
        private System.Windows.Forms.Button YminusButton;
        private System.Windows.Forms.Button XminusButton;
        private System.Windows.Forms.Button ZplusButton;
        private System.Windows.Forms.Button ZplusplusButton;
        private System.Windows.Forms.Button ZminusButton;
        private System.Windows.Forms.Button ZminusminusButton;
        private System.Windows.Forms.Button HomeButton;
        private System.Windows.Forms.TextBox xStartField;
        private System.Windows.Forms.TextBox xEndField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox xStepField;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox YstepField;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox YendField;
        private System.Windows.Forms.TextBox yStartField;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox camerIDtextField;
        private System.Windows.Forms.TextBox comPortTextField;
    }
}

