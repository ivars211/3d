﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3dControl
{
    public partial class Form1 : Form
    {
        SerialPort _port;
        string _moveCMD = "G0";
        bool _connected;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            if (comPortTextField.Text == "")
            {
                MessageBox.Show("No com port");
                return;
            }

            if (!_connected)
            {
                string portname = comPortTextField.Text;
                int baudrate = 115200;

                try
                {
                    // open port and wait for Arduino to boot
                    _port = new SerialPort(portname, baudrate);
                    _port.Open();
                    _port.NewLine = "\n";
                    _port.DtrEnable = true;
                    _port.RtsEnable = true;
                    _connected = true;
                    ConnectButton.Text = "Disconnect";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("SENDG: Cannot open serial port (Portname=" + portname + ")");
                    _connected = false;
                }
            }
            else
            {
                _port.Close();
                _connected = false;
                ConnectButton.Text = "Connect";
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            scan(true);
        }
        private void scan(bool single)
        {
            if (!_connected)
            {
                MessageBox.Show("Not connected!");
                return;
            }
            if (xStartField.Text == "" || xEndField.Text == "" || xStepField.Text == "" || yStartField.Text == "" || YendField.Text == "" || YstepField.Text == "")
            {
                MessageBox.Show("Missing values!");
                return;
            }
            _port.Write("G90".Trim() + "\n");
            double xStart = double.Parse(xStartField.Text);
            double xEnd = double.Parse(xEndField.Text);
            double xStep = double.Parse(xStepField.Text);

            double yStart = double.Parse(yStartField.Text);
            double yEnd = double.Parse(YendField.Text);
            double yStep = double.Parse(YstepField.Text);

            double stepSizeX = (xEnd - xStart) / xStep;
            double stepSizeY = (yEnd - yStart) / yStep;
            int aaaa = 0;
            for (double x = xStart; x <= xEnd; x += stepSizeX)
            {
                if (aaaa % 2 == 0)
                {
                    for (double y = yStart; y <= yEnd; y += stepSizeY)
                    {
                        string cmd = "G0 X" + x.ToString("#.##") + " Y" + y.ToString("#.##") + " F3000".Trim() + "\n";
                        _port.Write(cmd);

                        Console.WriteLine(cmd);
                        Thread.Sleep(5000);
                        takePictures(single);
                    }
                }
                else
                {
                    for (double y = yEnd; y >= yStart; y -= stepSizeY)
                    {
                        string cmd = "G0 X" + x.ToString("#.##") + " Y" + y.ToString("#.##") + " F3000".Trim() + "\n";
                        _port.Write(cmd);

                        Console.WriteLine(cmd);
                        Thread.Sleep(5000);
                        takePictures(single);
                    }
                }
                aaaa++;
            }
        }
        private void HomeButton_Click(object sender, EventArgs e)
        {
            if (!_connected)
            {
                MessageBox.Show("Not connected!");
                return;
            }
            _port.Write("G28 X".Trim() + "\n");
            _port.Write("G0 Z0 X140".Trim() + "\n");
            Thread.Sleep(5000);
            _port.Write("G28 Y".Trim() + "\n");
            _port.Write("G0 Z0 Y100".Trim() + "\n");
        }
        void move(string cmd)
        {
            if (!_connected)
            {
                MessageBox.Show("Not connected!");
                return;
            }
            _port.Write("G91".Trim() + "\n");
            string partial = _moveCMD + " " + cmd;
            _port.Write(partial.Trim() + "\n");
        }

        private void XminusButton_Click(object sender, EventArgs e)
        {
           
            move("X-10");
        }

        private void XplusButton_Click(object sender, EventArgs e)
        {
            move("X10");
        }

        private void YplusButton_Click(object sender, EventArgs e)
        {
            move("Y10");

        }

        private void YminusButton_Click(object sender, EventArgs e)
        {
            move("Y-10");

        }

        private void ZplusButton_Click(object sender, EventArgs e)
        {
            move("Z0.1");
        }

        private void ZplusplusButton_Click(object sender, EventArgs e)
        {
            move("Z5");
        }
       

        private void ZminusButton_Click(object sender, EventArgs e)
        {
            move("Z-0.1");
        }

        private void ZminusminusButton_Click(object sender, EventArgs e)
        {

            move("Z-5");
        }
        private void takePictures(bool single)
        {
            if (single)
            {
                int cameraID = int.Parse(camerIDtextField.Text);
                File.WriteAllText(cameraID + ".txt", "1");
                Thread.Sleep(3000);
            }
            else
            {
                for (int i = 0; i < 6; i++)
                {
                    File.WriteAllText(i + ".txt", "1");
                    Thread.Sleep(5000);
                }
            }
      
        }

        private void button1_Click(object sender, EventArgs e) //3d
        {
            scan(false);
            
        }
    }
}
